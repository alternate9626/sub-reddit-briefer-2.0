﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubRedditBriefer_2._0
{
    interface IOManager
    {
        void showWelcomeMessage();
        String getUserInput();
        void showPressEnterToContinue();
        void showSubRedditName(String URL);
        void showRedditPosts(List<RedditPost> posts);
    }
}
