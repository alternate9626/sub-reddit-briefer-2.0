﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubRedditBriefer_2._0
{
    class ConsoleIOManager : IOManager
    {
        public void showWelcomeMessage()
        {
            Console.WriteLine("Welcome to my SubRedditBriefer program! (Name subject to change, idk)");
        }

        public String getUserInput()
        {
            return Console.ReadLine();
        }

        public void showPressEnterToContinue()
        {
            Console.WriteLine("Press enter to Continue!");
            Console.ReadLine();
        }

        public void showSubRedditName(String URL)
        {
            Console.WriteLine("This is the subreddit link you want to brief: \n" + URL);
        }

        public void showRedditPosts(List<RedditPost> posts)
        {            
            foreach (RedditPost post in posts)
            {
                this.showTitle(post.title);
                this.showAuthor(post.author);
                this.showDate(post.date);
                this.showCommentCount(post.commentsCount.Trim());
                this.showLineDivisor();
            }            
        }

        private void showTitle(String title)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(title + " ");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void showAuthor(String author)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write(author + " ");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void showDate(String date)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(date + " ");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void showCommentCount(String commentCount)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(commentCount + " Comments. ");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void showLineDivisor()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("-------------------------------------------------");
        }
    }
}
