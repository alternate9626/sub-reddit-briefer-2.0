﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubRedditBriefer_2._0
{
    class RedditPost
    {
        public String title, author, date, commentsCount;
        public RedditPost(String title, String author,String date, String commentsCount)
        {
            this.title = title;
            this.author = author;
            this.date = date;
            this.commentsCount = commentsCount;
        }
    }
}
