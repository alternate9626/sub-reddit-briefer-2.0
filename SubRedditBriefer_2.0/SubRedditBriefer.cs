﻿using RedditSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubRedditBriefer_2._0
{
    class SubRedditBriefer
    {
        IOManager ioManager;
        String subReddit;
        Reddit reddit;

        public SubRedditBriefer(IOManager ioManager)
        {
            this.ioManager = ioManager;
            this.reddit = new Reddit();
        }

        public void Start(String subReddit)
        {
            this.subReddit = subReddit;
            ioManager.showWelcomeMessage();            
            ioManager.showSubRedditName(this.subReddit);
            ioManager.showRedditPosts(getPosts());
            ioManager.showPressEnterToContinue();
        }

        private List<RedditPost> getPosts()
        {

            List<RedditPost> redditPosts = new List<RedditPost>();
            
            var subReddit = this.reddit.GetSubreddit(this.subReddit);
            foreach (var post in subReddit.New.Take(25))
            {
                redditPosts.Add(new RedditPost(post.Title, post.AuthorName, post.Created.ToString(), post.CommentCount.ToString()));              
            }
                return redditPosts;
        }
    }
}
