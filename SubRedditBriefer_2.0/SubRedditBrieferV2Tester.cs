﻿using RedditSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubRedditBriefer_2._0
{
    class SubRedditBrieferV2Tester
    {
        static void Main(string[] args)
        {
            String subReddit = "/r/ultrawidemasterrace";
            IOManager ioManager = new ConsoleIOManager();
            SubRedditBriefer briefer = new SubRedditBriefer(ioManager);
            briefer.Start(subReddit);        
        }
    }
}
